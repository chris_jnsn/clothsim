IF (MINGW)

	set(ASSIMP_INCLUDE_PATH "${CMAKE_SOURCE_DIR}/dependencies/assimp/include/")
	set(ASSIMP_LIB "${CMAKE_SOURCE_DIR}/dependencies/assimp/libMinGW/libassimp.a")

ELSEIF (MSVC)

    set(ASSIMP_INCLUDE_PATH "${CMAKE_SOURCE_DIR}/dependencies/assimp/include/")
    set(ASSIMP_LIB "${CMAKE_SOURCE_DIR}/dependencies/assimp/lib/assimp_release-dll_win32/assimp.lib")

ELSEIF("${CMAKE_SYSTEM}" MATCHES "Linux")
	
  FIND_PATH(ASSIMP_INCLUDE_PATH assimp/defs.h)
  FIND_LIBRARY(ASSIMP_LIBRARY
       NAMES assimp
	)

ENDIF ()


