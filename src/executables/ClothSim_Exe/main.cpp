//TODO

// 1. fix pole position depending on the cloth size
// 2. sinus wave to wind force
// 3. sphere collision

#include <CVK_2/CVK_Framework.h>

#include <CVK_AnSim/CVK_AS_ShaderLineRender.h>
#include <CVK_AnSim/CVK_AS_Line.h>
#include <CVK_AnSim/CVK_AS_LineStrip.h>
#include <CVK_AnSim/CVK_AS_CoordinateSystem.h>

#include "ShaderClothSim.h"
#include "ShaderClothRendering.h"
#include "GUI.h"

#include <iostream>
#include <fstream>

GUI *m_gui;

//window
#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 800

GLFWwindow *window;

// define Nodes ==============================================================================
CVK::Node *scene_node;
CVK::Node *cloth_node;

// sphere collision ==========================================================================
CVK::Node *sphere_node;
float sphereRadius = 0.5f;
glm::fvec3 spherePos = glm::fvec3(3.5f, 1.9f, -0.5f);
bool sphereCollision = false;

// ===========================================================================================

CVK::Geometry *cloth_geometry;
CVK::Node *sfq_node;

// define cloth variables ====================================================================
const int CLOTH_WIDTH = 43;
const int CLOTH_HEIGHT = 43;

const int TEXTURE_ARRAY_SIZE = CLOTH_WIDTH * CLOTH_HEIGHT * 4;
bool wireframeMode = false;

//fbos for position textures==================================================================
CVK::FBO *fboPrev;
CVK::FBO *fboCurr;
CVK::FBO *fboNext;

//define Camera (Trackball)==================================================================
CVK::Ortho sfqOrtho(0.0f, 1.0f, 0.0f, 1.0f, 0.1f, 100.0f);
CVK::Perspective projection(60.0f, WINDOW_WIDTH / (float) WINDOW_HEIGHT, 1.0f, 1000.f);
CVK::Trackball cam_trackball(WINDOW_WIDTH, WINDOW_HEIGHT, &projection);
CVK::Trackball cam_cloth(CLOTH_WIDTH, CLOTH_HEIGHT, &sfqOrtho);

//define light and materials==================================================================
CVK::Light *plight;
float shininess = 100.0f;
CVK::Material *mat_grey, *mat_red, *mat_floor, *mat_cvlogo, *mat_white;

//define variables for GUI====================================================================
bool fTenPressed = false;
int fps;
double renderTime;

//adapt viewport to window size===============================================================
void resizeCallback(GLFWwindow *window, int w, int h) {
    cam_trackball.setWidthHeight(w, h);
    cam_trackball.getProjection()->updateRatio(w / (float) h);
    glViewport(0, 0, w, h);
}

void init_camera() {
    glm::vec3 v(0.0f, 0.0f, 0.0f);
    cam_cloth.setCenter(&v);
    cam_cloth.setRadius(15);
    CVK::State::getInstance()->setCamera(&cam_cloth);

    cam_trackball.setCenter(&v);
    cam_trackball.setRadius(15);
    CVK::State::getInstance()->setCamera(&cam_trackball);
}

void init_materials() {
    mat_grey = new CVK::Material(grey, white, shininess);
    mat_red = new CVK::Material(redCol, white, shininess);
    mat_white = new CVK::Material(white, white, shininess);
    mat_floor = new CVK::Material(TEXTURES_PATH "/granite_diffuse.jpg", 1.0f, 0.27f, white, shininess);
    mat_cvlogo = new CVK::Material(TEXTURES_PATH "/cv_logo.bmp", 1.0f, 0.75f, white, shininess);
}

void init_scene() {
    scene_node = new CVK::Node("Scene");

    const float flagYSize = static_cast<float>(CLOTH_HEIGHT) / 20.0f;

    CVK::Node *flagpole_node = new CVK::Node("flagpole_up");
    flagpole_node->setModelMatrix(glm::translate(*flagpole_node->getModelMatrix(), glm::vec3(0, 0, 0)));
    flagpole_node->setModelMatrix(glm::scale(*flagpole_node->getModelMatrix(), glm::vec3(0.1, 4, 0.1)));
    flagpole_node->setMaterial(mat_grey);
    CVK::Cube *cube = new CVK::Cube();
    flagpole_node->setGeometry(cube);
    scene_node->addChild(flagpole_node);

    CVK::Node *plane_node_up = new CVK::Node("plane_up");
    plane_node_up->setModelMatrix(
    glm::translate(*plane_node_up->getModelMatrix(), glm::vec3(0, -4, 0)));
    plane_node_up->setModelMatrix(glm::scale(*plane_node_up->getModelMatrix(), glm::vec3(5, 5, 5)));

    plane_node_up->setMaterial(mat_floor);
    CVK::Plane *plane = new CVK::Plane();
    plane_node_up->setGeometry(plane);
    scene_node->addChild(plane_node_up);

    //CREATE COLLISION SPHERE ======================================================
   
    sphere_node = new CVK::Node("sphere_up");
    
    sphere_node->setMaterial(mat_red);
    CVK::Sphere *sphere = new CVK::Sphere(sphereRadius);
    sphere_node->setGeometry(sphere);
    //renders itself currently
    //scene_node->addChild(sphere_node);
    

    //CREATE SCREEN FILLING QUAD =============================================
    CVK::Plane *sfq_plane = new CVK::Plane();
    sfq_plane->set_Points(
        glm::vec3(0.f, 1.f, 0.f),
        glm::vec3(0.f, 0.f, 0.f),
        glm::vec3(1.f, 0.f, 0.f),
        glm::vec3(1.f, 1.f, 0.f));

    sfq_plane->set_Tcoords(
        glm::vec2(0.f, 1.f),
        glm::vec2(0.f, 0.f),
        glm::vec2(1.f, 0.f),
        glm::vec2(1.f, 1.f)
        );

    
    sfq_node = new CVK::Node("sfqNode");
    sfq_node->setGeometry(sfq_plane);

    //CREATE CLOTH MESH ======================================================

    cloth_node = new CVK::Node("cloth_up");

    cloth_geometry = new CVK::Geometry();
    std::vector<glm::vec4> *tmpVert1 = cloth_geometry->getVertices();
    std::vector<glm::vec3> *tmpNormal1 = cloth_geometry->getNormals();
    std::vector<glm::vec2> *tmpUV1 = cloth_geometry->getUVs();
    std::vector<unsigned int> *tmpIndex1 = cloth_geometry->getIndex();

    //add vertices, normals, uvs
    for (int y = 0; y < CLOTH_HEIGHT; y++) {
        for (int x = 0; x < CLOTH_WIDTH; x++) {

            tmpVert1->push_back(glm::fvec4((static_cast<float>(x) / static_cast<float>(CLOTH_WIDTH)), (static_cast<float>(y) / static_cast<float>(CLOTH_HEIGHT)), 0.0f, 1.0f));
            float curU = static_cast<float>(x) / static_cast<float>(CLOTH_WIDTH);
            float curV = static_cast<float>(y) / static_cast<float>(CLOTH_HEIGHT);
            tmpUV1->push_back(glm::fvec2(curU, curV));

            tmpNormal1->push_back(glm::fvec3(0.0f, 0.0f, 1.0f));
        }
    }

    //add indices
    for (int y = 0; y < CLOTH_HEIGHT - 1; y++) {
        for (int x = 0; x < CLOTH_WIDTH - 1; x++) {
            //face1
            tmpIndex1->push_back(y * (CLOTH_WIDTH) + x);
            tmpIndex1->push_back(y * (CLOTH_WIDTH) + (x + 1));
            tmpIndex1->push_back((y + 1) * (CLOTH_WIDTH) + x);

            //face2
            tmpIndex1->push_back((y + 1) * (CLOTH_WIDTH) + x);
            tmpIndex1->push_back(y * (CLOTH_WIDTH) + (x + 1));
            tmpIndex1->push_back((y + 1) * (CLOTH_WIDTH) + (x + 1));
        }
    }

    cloth_geometry->createBuffers();
    cloth_node->setGeometry(cloth_geometry);
    cloth_node->setMaterial(mat_cvlogo);
    cloth_node->setModelMatrix(glm::scale(glm::mat4(1.0f), glm::vec3(static_cast<float>(CLOTH_WIDTH) / 10.0f, static_cast<float>(CLOTH_HEIGHT) / 10.0f, 1.0f)));
}

void init_fbos() {

    //allocate memory for texture data
    float data1[TEXTURE_ARRAY_SIZE];
    float data2[TEXTURE_ARRAY_SIZE];
    float data3[TEXTURE_ARRAY_SIZE];

    //get vertices from geometry
    std::vector<glm::fvec4> *positions = cloth_geometry->getVertices();

    int index = 0;
    for (int h = 0; h < CLOTH_HEIGHT; h++) {
        for (int w = 0; w < CLOTH_WIDTH; w++) {

            //read positions from vertices and write them to the texture
            data1[index    ] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).r);
            data1[index + 1] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).g);
            data1[index + 2] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).b);
            data1[index + 3] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).a);

            data2[index] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).r);
            data2[index + 1] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).g);
            data2[index + 2] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).b);
            data2[index + 3] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).a);

            data3[index] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).r);
            data3[index + 1] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).g);
            data3[index + 2] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).b);
            data3[index + 3] = static_cast <float>(positions->at((h * CLOTH_WIDTH + w)).a);
            index += 4;
        }
    }

    //init fbo for previous, current and next positions
    fboPrev = new CVK::FBO(CLOTH_WIDTH, CLOTH_HEIGHT, 2);
    fboCurr = new CVK::FBO(CLOTH_WIDTH, CLOTH_HEIGHT, 2);
    fboNext = new CVK::FBO(CLOTH_WIDTH, CLOTH_HEIGHT, 2);

    //write data array to previous and current texture
    glBindTexture(GL_TEXTURE_2D, fboPrev->getColorTexture(0));
    glTexSubImage2D(GL_TEXTURE_2D,          //TEXTURE 2D
                    0,                      //MIP MAP LEVEL ZERO
                    0,                      //BOTTOM LEFT CORNER
                    0,
                    CLOTH_WIDTH,            //TOP RIGHT CORNER
                    CLOTH_HEIGHT,
                    GL_RGBA,                //FORMAT
                    GL_FLOAT,               //TYPE
                    data1                   //POINTER TO MEMORY WITH DATA
    );

    glBindTexture(GL_TEXTURE_2D, fboCurr->getColorTexture(0));
    glTexSubImage2D(GL_TEXTURE_2D,          //TEXTURE 2D
                    0,                      //MIP MAP LEVEL ZERO
                    0,                      //BOTTOM LEFT CORNER
                    0,
                    CLOTH_WIDTH,            //TOP RIGHT CORNER
                    CLOTH_HEIGHT,
                    GL_RGBA,                //FORMAT
                    GL_FLOAT,               //TYPE
                    data2                   //POINTER TO MEMORY WITH DATA
    );

    glBindTexture(GL_TEXTURE_2D, fboNext->getColorTexture(0));
    glTexSubImage2D(GL_TEXTURE_2D,      //TEXTURE 2D
        0,                              //MIP MAP LEVEL ZERO
        0,                              //BOTTOM LEFT CORNER
        0,
        CLOTH_WIDTH,                    //TOP RIGHT CORNER
        CLOTH_HEIGHT,
        GL_RGBA,                        //FORMAT
        GL_FLOAT,                       //TYPE
        data3                           //POINTER TO MEMORY WITH DATA
        );
    
    glBindTexture(GL_TEXTURE_2D, 0);

}

void init_gui()
{
    m_gui = new GUI();
    m_gui->setFPSVariable(&fps, &renderTime);
    m_gui->setWireframeVariable(&wireframeMode);
    m_gui->setSphereCollisionVariables(&sphereCollision, &spherePos);
}

int main() {
    // Init GLFW and GLEW
    glfwInit();
    CVK::useOpenGL33CoreProfile();
    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "ClothSim", 0, 0);
    glfwSetWindowPos(window, 100, 50);
    glfwMakeContextCurrent(window);
    glfwSetWindowSizeCallback(window, resizeCallback);
    glewInit();
	init_gui();

    CVK::State::getInstance()->setBackgroundColor(darkgrey);
    glm::vec3 BgCol = CVK::State::getInstance()->getBackgroundColor();
    glClearColor(BgCol.r, BgCol.g, BgCol.b, 0.0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


    //init shader for scene
    const char *phongShaderNames[2] = { SHADERS_PATH "/Phong.vert", SHADERS_PATH "/Phong.frag" };
    CVK::ShaderPhong phongShader(VERTEX_SHADER_BIT | FRAGMENT_SHADER_BIT, phongShaderNames);
    CVK::State::getInstance()->setShader(&phongShader);

    //init shader for cloth rendering
    const char *shadernamesClothRendering[3] = { SHADERS_PATH "/ClothRendering.vert", SHADERS_PATH "/ClothRendering.geom", SHADERS_PATH "/ClothRendering.frag" };
	ShaderClothRendering clothRenderingShader(VERTEX_SHADER_BIT | GEOMETRY_SHADER_BIT | FRAGMENT_SHADER_BIT, shadernamesClothRendering, m_gui);
    CVK::State::getInstance()->setShader(&clothRenderingShader);
      
    // init shader for clothSim
    const char *shadernamesClothSim[2] = { SHADERS_PATH "/ClothSim.vert", SHADERS_PATH "/ClothSim.frag" };
    ShaderClothSim clothSimShader(VERTEX_SHADER_BIT | FRAGMENT_SHADER_BIT, shadernamesClothSim, static_cast<float>(CLOTH_WIDTH),static_cast<float>(CLOTH_HEIGHT), 1.0f / static_cast<float>(CLOTH_WIDTH), 1.0f / static_cast<float>(CLOTH_HEIGHT), m_gui);
    CVK::State::getInstance()->setShader(&clothSimShader);

    init_camera();
    init_materials();
    init_scene();
    init_fbos();

    //define Light Sources
    plight = new CVK::Light(glm::vec4(-1, 4, 1, 1), grey, glm::vec3(0, 0, 0), 1.0f, 0.0f);
    CVK::State::getInstance()->addLight(plight);
    CVK::State::getInstance()->updateSceneSettings(darkgrey, 0, white, 1, 10, 1);

    float currentTime = static_cast<float>(glfwGetTime());

    while (!glfwWindowShouldClose(window)) {
        //delta time
        float newTime = static_cast<float>(glfwGetTime());
        float deltaT = newTime - currentTime;
        currentTime = newTime;

        fps = static_cast<int>(1.0f / deltaT);

        
        if (glfwGetKey(window, GLFW_KEY_ESCAPE))
            break; //quit

        if (glfwGetKey(window, GLFW_KEY_F10))
            fTenPressed = true;

        if (fTenPressed && !glfwGetKey(window, GLFW_KEY_F10)) {
            if (m_gui->isVisible())
                m_gui->hide();
            else
                m_gui->show();

            fTenPressed = false;
        }


        
        ///CLOTH CALC===============================================
        
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
        
        //set orthographic projection
        CVK::State::getInstance()->setCamera(&cam_cloth);
                
        //bind fbo to save new positions
        fboNext->bind();
        glClear(GL_COLOR_BUFFER_BIT);

        CVK::State::getInstance()->setShader(&clothSimShader);
        clothSimShader.update(fboPrev->getColorTexture(0), fboCurr->getColorTexture(0), newTime);

        //update sphere position
        sphere_node->setModelMatrix(glm::translate(glm::mat4(1.0f), spherePos));
        clothSimShader.updateSphereCollision(sphereRadius, spherePos, sphereCollision, *cloth_node->getModelMatrix(), *sphere_node->getModelMatrix());

        //render screen filling quad
        sfq_node->render();
        fboNext->unbind();

        //restore viewport
        glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
      

        ///=========================================================

        
        ///RENDERING================================================
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
         
        glEnable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
       
        //Update Camera
        CVK::State::getInstance()->setCamera(&cam_trackball);
              
		//250 is the width of the gui
		cam_trackball.update(window);

        //render scene with phong shading
        CVK::State::getInstance()->setShader(&phongShader);
        phongShader.update();
        CVK::State::getInstance()->setLight(0, plight);

        scene_node->render();

        //only render sphere if option is set in GUI
        if (sphereCollision)
            sphere_node->render();

        //render cloth with cloth rendering shader
        glm::mat4 MVP = *projection.getProjMatrix() * *cam_trackball.getView() * *cloth_node->getModelMatrix();
        CVK::State::getInstance()->setShader(&clothRenderingShader);
		clothRenderingShader.update(fboNext->getColorTexture(0), fboNext->getColorTexture(1), MVP);
        
        if (wireframeMode)
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        cloth_node->render();
        
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        m_gui->render(window);
        
        ///=========================================================

        renderTime = glfwGetTime() - currentTime;

        //Switch FBO Pointers for next rendering step  
        CVK::FBO *tmp = fboPrev;
        fboPrev = fboCurr;
        fboCurr = fboNext;
        fboNext = tmp;
        
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}

