#pragma once
#include<string>
#include<vector>
#include<CVK_2/CVK_Texture.h>

class GUI
{
  public:
    GUI();
    ~GUI();

    void show();
    void hide();

    inline bool isVisible() { return m_visible; }

    void render(GLFWwindow* window);
    void setFPSVariable(int *finalFPS, double *renderTime);
    void setWireframeVariable(bool *wire);
	void setWindwaveVariable(bool *wind);
	void setSpeedVariable(float *speed, float *speedSquared);
	void setDefaultDampingVariable(float *defaultDamping);
	void setStructSpringVariable(float *structSpring);
	void setStructDampingVariable(float *structDamping);
	void setShearSpringVariable(float *shearSpring);
	void setShearDampingVariable(float *shearDamping);
	void setBendSpringVariable(float *bendSpring);
	void setBendDampingVariable(float *bendDamping);
	void setGravityVariable(glm::fvec3 *gravity);
	void setPointMassVariable(float *pointMass);
    void setWindForceVariable(glm::fvec3 *windForce);
	void setNormalsVariable(bool *normals);
    void setSphereCollisionVariables(bool *renderSphere, glm::fvec3 *pos);

  private:
    void initGUI();
    void update(GLFWwindow* window);
    void loadFonts();
    void drawElements(GLFWwindow* window);
    void tab();
    std::string doubleDigitToString(double& number);


    bool m_visible;
    bool m_initialized;

    CVK::Texture *m_headerTexture;

	  //Force Variables
	  float *m_structSpring;
	  float *m_structDamping;
	  float *m_shearSpring;
	  float *m_shearDamping;
	  float *m_bendSpring;
	  float *m_bendDamping;
	  float *m_defaultDamping;
	  float *m_pointMass;

	  float *m_gravity;
      glm::fvec3 *m_windForce;

	  //Timestep Variable
	  float *m_speed;
	  float *m_speedSquared;

	  //Performance
      int *m_finalFPS;
      double *m_renderTime;

	  //RENDERER INFO
	  std::string m_renderTextA;
	  std::string m_renderTextB;
	  std::string m_renderTextC;

      //Visualisation

      bool *m_wireframe;
	  bool *m_normals;
	  bool *m_windwave;

      //Sphere Collision
      glm::fvec3 *m_spherePosition;
      bool *m_renderSphere;
};
