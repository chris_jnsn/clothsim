#include "ShaderClothSim.h"


float rand_FloatRange(float a, float b)
{
	return ((b - a)*((float)rand() / RAND_MAX)) + a;
}


ShaderClothSim::ShaderClothSim(GLuint shader_mask, const char** shaderPaths, float clothWidth, float clothHeight, float clothPatchWidth, float clothPatchHeight, GUI *clothGui)
	: CVK::ShaderMinimal(shader_mask, shaderPaths)
{
    m_posPrevID           = glGetUniformLocation(m_ProgramID, "posPrev");
    m_posCurrID           = glGetUniformLocation(m_ProgramID, "posCurr");
    m_timeStepID          = glGetUniformLocation(m_ProgramID, "timeStep");
    m_timeStepSquaredID   = glGetUniformLocation(m_ProgramID, "timeStepSquared");
    m_pointMassID         = glGetUniformLocation(m_ProgramID, "pointMass");
    
    m_clothWidthID        = glGetUniformLocation(m_ProgramID, "clothWidth");
    m_clothHeightID       = glGetUniformLocation(m_ProgramID, "clothHeight");
    m_clothPatchSizeID    = glGetUniformLocation(m_ProgramID, "clothPatchSize");
    m_texelSizeID         = glGetUniformLocation(m_ProgramID, "texelSize");
    
    m_defaultDampingID    = glGetUniformLocation(m_ProgramID, "defaultDamping");
    m_structSpringID      = glGetUniformLocation(m_ProgramID, "structSpring");
    m_structDampingID     = glGetUniformLocation(m_ProgramID, "structDamping");
    m_shearSpringID       = glGetUniformLocation(m_ProgramID, "shearSpring");
    m_shearDampingID      = glGetUniformLocation(m_ProgramID, "shearDamping");
    m_bendSpringID        = glGetUniformLocation(m_ProgramID, "bendSpring");
    m_bendDampingID       = glGetUniformLocation(m_ProgramID, "bendDamping");
    m_gravityID           = glGetUniformLocation(m_ProgramID, "gravity");
    m_windForceID         = glGetUniformLocation(m_ProgramID, "windForce");

    m_spherePositionID      = glGetUniformLocation(m_ProgramID, "spherePosition");
    m_sphereRadiusID        = glGetUniformLocation(m_ProgramID, "sphereRadius");
    m_useSphereCollisionID  = glGetUniformLocation(m_ProgramID, "useSphereCollision");
    m_clothModelMatrixID    = glGetUniformLocation(m_ProgramID, "sphereModelMatrix");
    m_sphereModelMatrixID   = glGetUniformLocation(m_ProgramID, "clothModelMatrix");


    m_timeStep            = 1.0f / 30.0f;
    m_timeStepSquared     = m_timeStep * m_timeStep;
    m_pointMass           = 1.0f;

    m_clothWidth          = clothWidth;
    m_clothHeight         = clothHeight;
    m_clothPatchSize      = glm::fvec2(clothPatchWidth, clothPatchHeight);
    m_texelSize           = glm::fvec2(1.0f / (m_clothWidth - 1.0f), 1.0f / (m_clothHeight - 1.0f));
    m_gravity             = glm::fvec3(0.0f, -0.00981f, 0.0f);
    m_windForce           = glm::fvec3(0.035f, 0.0f, -0.005f);
	m_sinWindForce		  = glm::fvec3(0.5f, 0.0f, 0.0f);
	m_sin				  = 0.0f;
	m_windMode			  = false;

    //Force variables
    m_defaultDamping      = - 0.0125f;
    m_structSpring        =  50.75f;
    m_structDamping       = -0.25f;
    m_shearSpring         =  50.75f;
    m_shearDamping        = -0.25f;
    m_bendSpring          =  50.95f;
    m_bendDamping         = -0.25f;

    m_screenFillingQuad = CVK::Plane();
    m_screenFillingQuad.set_Points(
        glm::vec3(-1.f, 1.f, 0.f),
        glm::vec3(-1.f, -1.f, 0.f),
        glm::vec3(1.f, -1.f, 0.f),
        glm::vec3(1.f, 1.f, 0.f));

	clothGui->setSpeedVariable(&m_timeStep, &m_timeStepSquared);
	clothGui->setDefaultDampingVariable(&m_defaultDamping);
	clothGui->setStructSpringVariable(&m_structSpring);
	clothGui->setStructDampingVariable(&m_structDamping);
	clothGui->setShearSpringVariable(&m_shearSpring);
	clothGui->setShearDampingVariable(&m_shearDamping);
	clothGui->setBendSpringVariable(&m_bendSpring);
	clothGui->setBendDampingVariable(&m_bendDamping);
	clothGui->setGravityVariable(&m_gravity);
	clothGui->setPointMassVariable(&m_pointMass);
    clothGui->setWindForceVariable(&m_windForce);
	clothGui->setWindwaveVariable(&m_windMode);
}

void ShaderClothSim::update(GLuint posPrev, GLuint posCurr, float time)
{
	CVK::ShaderMinimal::update();
	//glUniform4fv(m_colorID, 1, glm::value_ptr(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f)));

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, posPrev);
  glUniform1i(m_posPrevID, 0);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, posCurr);
  glUniform1i(m_posCurrID, 1);

  glUniform1f(m_timeStepID,         m_timeStep);
  glUniform1f(m_timeStepSquaredID,  m_timeStepSquared);
  glUniform1f(m_pointMassID,        m_pointMass);

  glUniform1f(m_clothWidthID,       m_clothWidth);
  glUniform1f(m_clothHeightID,      m_clothHeight);
  glUniform2fv(m_clothPatchSizeID, 1, glm::value_ptr(m_clothPatchSize));
  glUniform2fv(m_texelSizeID,      1, glm::value_ptr(m_texelSize));
 
  glUniform1f(m_defaultDampingID,   m_defaultDamping);
  glUniform1f(m_structSpringID,     m_structSpring);
  glUniform1f(m_structDampingID,    m_structDamping);
  glUniform1f(m_shearSpringID,      m_shearSpring);
  glUniform1f(m_shearDampingID,     m_shearDamping);
  glUniform1f(m_bendSpringID,       m_bendSpring);
  glUniform1f(m_bendDampingID,      m_bendDamping);
  glUniform3fv(m_gravityID, 1,      glm::value_ptr(m_gravity));
  if (!m_windMode)
  {
	  glUniform3fv(m_windForceID, 1, glm::value_ptr(m_windForce));
  }
  else
  {
	  //m_sinWindForce.x += 0.05;
	  //m_sinWindForce.y += 0.05;
	  m_sin += 0.005;
	  //m_sinWindForce.x = sin(m_sinWindForce.x);
	  //m_sinWindForce.y = sin(m_sinWindForce.y);
	  m_sinWindForce.z = sin(m_sin);
	  glUniform3fv(m_windForceID, 1, glm::value_ptr(m_sinWindForce));
  }
  
}

void ShaderClothSim::updateSphereCollision(float sphereRadius, glm::fvec3 spherePos, bool useSphereCollision, glm::mat4 clothMM, glm::mat4 sphereMM)
{
    glUniform1f(m_sphereRadiusID, sphereRadius);
    glUniform3fv(m_spherePositionID, 1, glm::value_ptr(spherePos));
    glUniform1i(m_useSphereCollisionID, static_cast<int>(useSphereCollision));
    glUniformMatrix4fv(m_clothModelMatrixID, 1, GL_FALSE, glm::value_ptr(clothMM));
    glUniformMatrix4fv(m_sphereModelMatrixID, 1, GL_FALSE, glm::value_ptr(sphereMM));
}

void ShaderClothSim::render()
{
  m_screenFillingQuad.render();
}

