#pragma once 
#include "CVK_2/CVK_Defs.h"
#include "CVK_2/CVK_Camera.h"
#include "CVK_2/CVK_Light.h"
#include "CVK_2/CVK_Material.h"
#include "CVK_2/CVK_Geometry.h"
#include "CVK_2/CVK_ShaderMinimal.h"
#include "GUI.h"

    class ShaderClothRendering : public CVK::ShaderMinimal
    {
    public:
		ShaderClothRendering(GLuint shader_mask, const char** shaderPaths, GUI *clothGui);
		void update(GLuint posTextureID, GLuint normalTextureID, glm::mat4 const & MVP);
        void update(CVK::Node* node);

    private:

        GLuint m_useColorTexture, m_colorTextureID;
        GLuint m_positionTextureID;
		GLuint m_normalTextureID;
        GLuint m_mvpID;

        GLuint m_kdID, m_ksID, m_ktID;
        GLuint m_diffuseID, m_specularID, m_shininessID;
        GLuint m_lightambID;

        GLuint m_numLightsID;
        GLuint m_lightposID[MAX_LIGHTS], m_lightcolID[MAX_LIGHTS], m_lightsdirID[MAX_LIGHTS], m_lightsexpID[MAX_LIGHTS], m_lightscutID[MAX_LIGHTS];

        GLuint m_fogcolID, m_fogstartID, m_fogendID, m_fogdensID, m_fogmodeID;

		GLuint m_normalsID;
		bool m_normals;
    };

