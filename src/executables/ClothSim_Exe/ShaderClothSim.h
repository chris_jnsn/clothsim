#pragma once

#include "CVK_2/CVK_ShaderMinimal.h"
#include "CVK_2/CVK_Plane.h"
#include "GUI.h"

class ShaderClothSim : public CVK::ShaderMinimal
{
public:
    ShaderClothSim(GLuint shader_mask, const char** shaderPaths, float clothWidth, float clothHeight, float clothPatchWidth, float clothPatchHeight, GUI *clothGui);

  void update(GLuint posPrev, GLuint posCurr, float time);
  void updateSphereCollision(float sphereRadius, glm::fvec3 spherePos, bool useSphereCollision, glm::mat4 clothMM, glm::mat4 sphereMM);

  void render();

private:

  //Textures for last and current Positions
  GLuint m_posPrevID;
  GLuint m_posCurrID;

  GLuint m_timeStepID;            float m_timeStep;
  GLuint m_timeStepSquaredID;     float m_timeStepSquared;
  GLuint m_pointMassID;           float m_pointMass;

  //world cloth size
  GLuint m_clothWidthID;          float m_clothWidth; 
  GLuint m_clothHeightID;         float m_clothHeight;
  GLuint m_clothPatchSizeID;      glm::fvec2 m_clothPatchSize;

  //distance to neighbouring texels
  GLuint m_texelSizeID;           glm::fvec2 m_texelSize;
 
  //forces
  GLuint m_defaultDampingID;      float m_defaultDamping;
  GLuint m_structSpringID;        float m_structSpring;
  GLuint m_structDampingID;       float m_structDamping;
  GLuint m_shearSpringID;         float m_shearSpring;
  GLuint m_shearDampingID;        float m_shearDamping;
  GLuint m_bendSpringID;          float m_bendSpring;
  GLuint m_bendDampingID;         float m_bendDamping;
  GLuint m_gravityID;             glm::fvec3 m_gravity;
  GLuint m_windForceID;           glm::fvec3 m_windForce;

  //sphere collision
  GLuint m_spherePositionID;
  GLuint m_sphereRadiusID;
  GLuint m_useSphereCollisionID; 
  GLuint m_clothModelMatrixID;
  GLuint m_sphereModelMatrixID;

  //wind mode
  bool m_windMode;
  float m_sin;
  glm::fvec3 m_sinWindForce;
  CVK::Plane m_screenFillingQuad;
};


