#include "ShaderClothRendering.h"

ShaderClothRendering::ShaderClothRendering(GLuint shader_mask, const char** shaderPaths, GUI *clothGui) : CVK::ShaderMinimal(shader_mask, shaderPaths)
{
    //Material
    m_kdID = glGetUniformLocation(m_ProgramID, "mat.kd");
    m_ksID = glGetUniformLocation(m_ProgramID, "mat.ks");
    m_ktID = glGetUniformLocation(m_ProgramID, "mat.kt");

    m_diffuseID = glGetUniformLocation(m_ProgramID, "mat.diffColor");
    m_specularID = glGetUniformLocation(m_ProgramID, "mat.specColor");
    m_shininessID = glGetUniformLocation(m_ProgramID, "mat.shininess");

    //Light
    m_numLightsID = glGetUniformLocation(m_ProgramID, "numLights");
    for (unsigned int i = 0; i < MAX_LIGHTS; i++)
    {
        char string[100];
        sprintf(string, "light[%d].pos", i);
        m_lightposID[i] = glGetUniformLocation(m_ProgramID, string);
        sprintf(string, "light[%d].col", i);
        m_lightcolID[i] = glGetUniformLocation(m_ProgramID, string);
        sprintf(string, "light[%d].spot_direction", i);
        m_lightsdirID[i] = glGetUniformLocation(m_ProgramID, string);
        sprintf(string, "light[%d].spot_exponent", i);
        m_lightsexpID[i] = glGetUniformLocation(m_ProgramID, string);
        sprintf(string, "light[%d].spot_cutoff", i);
        m_lightscutID[i] = glGetUniformLocation(m_ProgramID, string);
    }

    //Textures
    m_useColorTexture = glGetUniformLocation(m_ProgramID, "useColorTexture");
    m_positionTextureID = glGetUniformLocation(m_ProgramID, "positionTexture");
    
	m_normalTextureID = glGetUniformLocation(m_ProgramID, "normalTexture");
    m_colorTextureID = glGetUniformLocation(m_ProgramID, "colorTexture");
    m_mvpID = glGetUniformLocation(m_ProgramID, "MVP");

    //Fog
    m_lightambID = glGetUniformLocation(m_ProgramID, "lightAmbient");

	//Normals
	m_normals = false;
	m_normalsID = glGetUniformLocation(m_ProgramID, "showNormals");

	clothGui->setNormalsVariable(&m_normals);
}

void ShaderClothRendering::update(GLuint posTextureID, GLuint normalTextureID, glm::mat4 const & MVP)
{
    int numLights = CVK::State::getInstance()->getLights()->size();
    CVK::ShaderMinimal::update();

    glUniform1i(m_numLightsID, numLights);
    for (unsigned int i = 0; i < numLights; i++)
    {
        CVK::Light *light = &CVK::State::getInstance()->getLights()->at(i);
        glUniform4fv(m_lightposID[i], 1, glm::value_ptr(*light->getPosition()));
        glUniform3fv(m_lightcolID[i], 1, glm::value_ptr(*light->getColor()));
        glUniform3fv(m_lightsdirID[i], 1, glm::value_ptr(*light->getSpotDirection()));
        glUniform1f(m_lightsexpID[i], light->getSpotExponent());
        glUniform1f(m_lightscutID[i], light->getSpotCutoff());
    }

    glUniform3fv(m_lightambID, 1, glm::value_ptr(CVK::State::getInstance()->getLightAmbient()));
        
    glUniformMatrix4fv(m_mvpID, 1, GL_FALSE, glm::value_ptr(MVP));

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, posTextureID);
    glUniform1i(m_positionTextureID, 0);


	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normalTextureID);
	glUniform1i(m_normalTextureID, 1);

	glUniform1i(m_normalsID, m_normals);
}

void ShaderClothRendering::update(CVK::Node* node)
{

    if (node->hasMaterial())
    {
        CVK::Material* mat = node->getMaterial();
        CVK::Texture *color_texture;

        glUniform1f(m_kdID, mat->getKd());
        glUniform1f(m_ksID, mat->getKs());
        glUniform1f(m_ktID, mat->getKt());
        glUniform3fv(m_diffuseID, 1, glm::value_ptr(*mat->getdiffColor()));
        glUniform3fv(m_specularID, 1, glm::value_ptr(*mat->getspecColor()));
        glUniform1f(m_shininessID, mat->getShininess());

        bool colorTexture = mat->hasTexture(CVK::COLOR_TEXTURE);
        glUniform1i(m_useColorTexture, colorTexture);

        if (colorTexture)
        {
            //TODO: COLOR_TEXTURE_UNIT
            glUniform1i(m_colorTextureID, 2);

            glActiveTexture(GL_TEXTURE2);
            color_texture = mat->getTexture(CVK::COLOR_TEXTURE);
            color_texture->bind();
        }
    }
}
