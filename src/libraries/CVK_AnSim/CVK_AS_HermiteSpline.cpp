#include "CVK_AS_HermiteSpline.h"
#include <iostream>

#define ONE_THIRD (1.0f / 3.0f)
#define THRESH 0.001f

CVK::HermiteSpline::HermiteSpline()
{
}

CVK::HermiteSpline::~HermiteSpline()
{
}

void CVK::HermiteSpline::generateRenderVertices()
{
	std::vector<HermiteSplineControlPoint*>::iterator cpIt = mControlPoints.begin();
	while(cpIt != mControlPoints.end() - 1){
		sampleHermiteSpline(*cpIt, *(cpIt + 1), 32); 
		cpIt++;
	}
}

void CVK::HermiteSpline::generateArcLengthTable(int resolution)
{

	//********************************************************************************
	//TODO Aufgabe 5 (a)
	//Hier muss die Erstellung der Bogenlaengentabelle programmiert werden.
	//********************************************************************************

}


void CVK::HermiteSpline::getParameterByArcLength(float length, float &u, int &patchNum){

	//********************************************************************************
	//TODO Aufgabe 5 (b)
	//Hier soll der Parameter u und der entsprechende Spline fuer eine gegebene Bogenlaenge
	//in der Tabelle gesucht. Ein binaeres Suchverfahren ist zu empfehlen.
	//********************************************************************************

}


void CVK::HermiteSpline::calculateFrenetFrame(float u, HermiteSplineControlPoint* c0, HermiteSplineControlPoint* c1, glm::vec3 &tangent, glm::vec3 &binormal, glm::vec3 &normal)
{

	//********************************************************************************
	//TODO Aufgabe 5 (c)
	//Hier soll der Frenet-Frame berechnet und in den uebergebenen Funktionsargumenten gespeichert werden.
	//Tipp: Sie brauchen noch die zweite Ableitung einer Bezier-Kurve ...
	//********************************************************************************


}

void CVK::HermiteSpline::sampleHermiteSpline(HermiteSplineControlPoint* c0, HermiteSplineControlPoint* c1, int numVerts)
{
	for(int i = 0; i < numVerts; i++)
	{	
		glm::vec3 position, tangent;
		evaluateHermiteSpline((float) i/(numVerts-1),c0, c1, position, tangent);
		mVertices.push_back(position);
	}
}

void CVK::HermiteSpline::evaluateHermiteSpline(float t, HermiteSplineControlPoint* c0, HermiteSplineControlPoint* c1, glm::vec3 &point, glm::vec3 &tangent)
{
	std::vector<glm::vec3> bp;
	bp.push_back(c0->getPosition());
	bp.push_back(c0->getPosition() + ONE_THIRD * c0->getDirection());
	bp.push_back(c1->getPosition() - ONE_THIRD * c1->getDirection());
	bp.push_back(c1->getPosition());
	deCasteljau(t, bp, point, tangent);

}

void CVK::HermiteSpline::deCasteljau(float t, std::vector<glm::vec3> &cp, glm::vec3 &point, glm::vec3 &tangent)
{
	if(cp.size() == 2)
	{
		point = (1-t) * cp.at(0) + t * cp.at(1);
		tangent = cp.at(1) - cp.at(0);
		tangent = glm::normalize(tangent);
	}
	else
	{
	std::vector<glm::vec3> ncp;
	std::vector<glm::vec3>::iterator it = cp.begin();
	while( it != cp.end()-1)
	{
		ncp.push_back((1-t) * (*it) + t * (*(it+1)));
		it ++;
	}
	deCasteljau(t, ncp, point, tangent);
	}
}
