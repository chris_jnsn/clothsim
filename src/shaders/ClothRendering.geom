#version 330
layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

uniform sampler2D positionTexture;  
uniform sampler2D normalTexture;

uniform mat4 MVP;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

in vec2 passUV[];

out vec3 passPosition;
out vec3 passNormal;
out vec2 passTcoord;
  
void main()
{	
  for(int i=0; i<3; i++)
  {
	vec4 newPos = MVP * texture(positionTexture,passUV[i]);
    gl_Position.xyzw = newPos.xyzw;
	passPosition = newPos.xyz;
	passTcoord = passUV[i];


	float translateX = newPos.x - passPosition.x;
	float translateY = newPos.y - passPosition.y;
	float translateZ = newPos.z - passPosition.z;

//	(1.0, 0.0, 0.0, tranlateX,
//	 0.0, 1.0, 0.0, tranlateY,
//	 0.0, 0.0, 1.0, tranlateZ,
//	 0.0, 0.0, 0.0, 1.0)

	mat4 finalModelMatrix = mat4(
							1.0, 0.0, 0.0, 0.0,						// first column (not row!)
							0.0, 1.0, 0.0, 0.0,						// second column
							0.0, 0.0, 1.0, 0.0,						// third column
							translateX, translateY, translateZ, 1.0	// fourth column
								  );

	passNormal = vec4(transpose(inverse(viewMatrix * finalModelMatrix)) * texture(normalTexture,passUV[i])).xyz;
	passNormal = normalize(passNormal);
    EmitVertex();
  }
  EndPrimitive();
}  
