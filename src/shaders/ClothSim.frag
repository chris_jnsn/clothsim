#version 330 

//PREVIOUS AND CURRENT POSITION
uniform sampler2D posPrev;
uniform sampler2D posCurr;

//LENGTH OF A TIME STEP
uniform float timeStep;		
uniform float timeStepSquared;		

//MASS OF ONE POINT
uniform float pointMass;

//CLOTH SIZE
uniform float clothWidth;
uniform float clothHeight;
uniform vec2  clothPatchSize;
uniform vec2  texelSize;

// FORCES
uniform float defaultDamping;
uniform float structSpring;
uniform float structDamping;
uniform float shearSpring;
uniform float shearDamping;
uniform float bendSpring;
uniform float bendDamping;
uniform vec3  gravity;
uniform vec3  windForce;

// SPHERE COLLISION
uniform vec3  spherePosition;
uniform float sphereRadius;
uniform int   useSphereCollision;
uniform mat4  clothModelMatrix;
uniform mat4  sphereModelMatrix;

in vec2 passUV;
 
//FBO OUTPUT 
layout(location = 0) out vec4 posNext; 
layout(location = 1) out vec4 normNext;

const vec2 neighbourCoordinates[12] = vec2[12](
							//STRUCTURAL SPRINGS
							vec2( 1, 0),
							vec2( 0,-1),
							vec2(-1, 0),
							vec2( 0, 1),
							
							//SHEAR SPRINGS
							vec2( 1,-1),
							vec2(-1,-1),
							vec2(-1, 1),
							vec2( 1, 1),
							
							//BEND SPRINGS
							vec2( 2, 0),
							vec2( 0,-2),
							vec2(-2, 0),
							vec2( 0, 2)
);


vec3 calcNormal(vec3 pos)
{
	float currentX = floor(passUV.s * clothWidth);
	float currentY = floor(passUV.t * clothHeight);

	//calculate coordinates of current neighbour point
	vec2 neighbourPointA = neighbourCoordinates[0];
	vec2 neighbourPointB = neighbourCoordinates[1];
			
	//border control point a
	if (((currentY + neighbourPointA.y) < 0.0) || ((currentY + neighbourPointA.y) > (clothHeight - 1.0)))
		return vec3(1.0, 0.0, 0.0);

	if (((currentX + neighbourPointA.x) < 0.0) || ((currentX + neighbourPointA.x) > (clothWidth - 1.0)))
		return vec3(1.0, 0.0, 0.0);

		//border control point a
	if (((currentY + neighbourPointB.y) < 0.0) || ((currentY + neighbourPointB.y) > (clothHeight - 1.0)))
		return vec3(1.0, 0.0, 0.0);

	if (((currentX + neighbourPointB.x) < 0.0) || ((currentX + neighbourPointB.x) > (clothWidth - 1.0)))
		return vec3(1.0, 0.0, 0.0);
				
	vec2 neighbourCoordsA = vec2(currentX + neighbourPointA.x, currentY + neighbourPointA.y) * texelSize;
	vec2 neighbourCoordsB = vec2(currentX + neighbourPointB.x, currentY + neighbourPointB.y) * texelSize;

	vec3 first = texture(posCurr, neighbourCoordsA).xyz - pos;
	vec3 second = texture(posCurr,neighbourCoordsB).xyz - pos;
	return normalize(vec3(cross(normalize(first),normalize(second))));

	//DEBUG
	//return vec3(1.0,0.0,0.0);
}

void main()
{ 

	//get last and current position
	vec3 prevPos 	= texture(posPrev, passUV.st).xyz;
	vec3 currPos	= texture(posCurr, passUV.st).xyz;
	
	//calculate last velocity
	vec3 prevVel = (currPos - prevPos) / timeStep;
	
	//calculate current point position in cloth
	float clothXPos = floor(passUV.s * clothWidth);
	float clothYPos = floor(passUV.t * clothHeight);
	float currentIndex = clothYPos * clothWidth + clothXPos;
	
	
	//anchor points
	if(currentIndex == 0 || currentIndex == clothWidth * (clothHeight-1.0) + 1)
	{
		posNext = vec4(currPos,1.0);
	}	
	
	else
	{
		//calculate current normal
		normNext = vec4(calcNormal(vec3(currPos)), 1.0);

		//calculate current wind force
		vec3 finalWind = normNext.xyz * dot(windForce, normNext.xyz);

		vec3 finalForce =  finalWind + gravity * pointMass + prevVel * defaultDamping;

		//calculate internal forces
		float springForce  = 0.0;
		float dampingForce = 0.0;
				
		for(int i = 0; i < 12; i++)
		{
			//set forces according to current spring type
			if(i < 4)
			{
				springForce  = structSpring;
				dampingForce = structDamping;
			}
			
			else if(i < 8)
			{
				springForce  = shearSpring;
				dampingForce = shearDamping;
			}
			
			else if(i < 12)
			{
				springForce  = bendSpring;
				dampingForce = bendDamping;
			}
			
			//calculate coordinates of current neighbour point
			vec2 neighbourDistance   = neighbourCoordinates[i];
			float neighbourDistanceX = neighbourDistance.x;
			float neighbourDistanceY = neighbourDistance.y;
			
			//border control
			if (((clothYPos + neighbourDistanceY) < 0.0) || ((clothYPos + neighbourDistanceY) > (clothHeight - 1.0)))
				continue;

			if (((clothXPos + neighbourDistanceX) < 0.0) || ((clothXPos + neighbourDistanceX) > (clothWidth - 1.0)))
				continue;
				
			vec2 neighbourCoords = vec2(clothXPos + neighbourDistanceX, clothYPos + neighbourDistanceY) * texelSize;
				
			//calculate rest length of spring
			float restLength = length(neighbourDistance*clothPatchSize);
			
			//current position and previous velocity of current neighbour
			vec3 neighbourCurrPos = texture(posCurr, neighbourCoords).xyz;
			vec3 neighbourLastVel = (neighbourCurrPos - texture(posPrev, neighbourCoords).xyz)/timeStep;
			
			//calculate differences between positions and velocities
			vec3 posDiff = currPos - neighbourCurrPos;
			vec3 velDiff = prevVel - neighbourLastVel;
			
			//current spring length
			float dist = length(posDiff);
									
			//calculate springforce and add to sum of forces
			float currentSpringForce  = -springForce * (dist-restLength);
			float currentDampingForce = dampingForce * (dot(velDiff, posDiff)/dist);		
			
			vec3 currentForce = (currentSpringForce + currentDampingForce) * normalize(posDiff);
			finalForce += currentForce;	
		}
		
		//calculate new acceleration
		vec3 nextAcc = finalForce / vec3(pointMass);
				   
		// verlet integration
		vec3 nextPos = 2.0 * currPos - prevPos + nextAcc * timeStepSquared;
		
		//sphere collision
		if(useSphereCollision == 1)
		{
		
			//check if current position is inside of sphere radius
			
			mat4 sphereMM = sphereModelMatrix;
			mat4 clothMM = clothModelMatrix;
			
			vec4 spherePos = vec4(spherePosition,1.0);
			vec4 clothPos =  sphereMM *	vec4(nextPos,1.0);
			
			vec3 distanceVec = clothPos.xyz - spherePos.xyz;
			
			vec4 testVec = vec4(nextPos,1.0) - (clothMM * spherePos); 
				 
			float distanceToSphere = length (distanceVec);
			if(distanceToSphere < sphereRadius)
			{
				//collision detected -> move posNext to sphere surface
				
				nextPos = currPos + (sphereRadius - distanceToSphere + 0.0001) * normalize(distanceVec);
				
				//nextPos = spherePos.xyz + (sphereRadius * normalize(-distanceVec));
				//nextPos = spherePos.xyz + (sphereRadius * normalize(vec3(vec4((nextPos - spherePosition),1.0)).xyz) );
				
				//nextPos = (sphereRadius - distanceToSphere) * distanceVec / distanceToSphere;
				//nextPos = currPos;
			}
			
			
		}
		
		posNext = vec4(nextPos,1.0);
	}	
}
