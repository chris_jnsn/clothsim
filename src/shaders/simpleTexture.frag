#version 330

in vec2 passUVCoord;

uniform sampler2D prevTex;
uniform sampler2D currTex;
uniform sampler2D nextTex;

out vec4 fragmentColor;

void main() {
    fragmentColor = texture(nextTex, passUVCoord);
}